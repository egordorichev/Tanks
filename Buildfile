all:
g++ -std=c++11 tanks.cpp -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer -o Tanks
macos:
g++ -I/usr/local/include -L/usr/local/lib -std=c++11 src/main.cpp src/tanks.cpp src/window.cpp -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer -o Tanks
linux:
g++ -std=c++11 tanks.cppp -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer -o Tanks
windows:
g++ -std=c++11 tanks.cpp -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer -o Tanks
install:
cp ./Tanks /usr/bin/
