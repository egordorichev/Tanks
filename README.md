# Tanks

Game about tanks written in c++

### Requirements

You need SDL2, SDL2-image, SDL2-ttf and SDL2-mixer libraries installed on your computer.

#### Instaling sdl

Run installsdl.sh

### Build

Open terminal and type:

```
cd /path/to/project/
make (macos|linux|windows)
```

### Run

Double click on file named "Tanks" or type in terminal:

```
cd /path/to/project/
./Tanks
```

### Install

Open terminal and type:

```
cd /path/to/project/
make install
```

## How to use

### Menu

Navigate menu by using Arrow keys and Enter.

### Game play

Use WASD buttons or Arrow keys to move your tank all over the map and space button to shoot.

After you kiling 50 tanks, you will see the <b>BOSS</b>! Have fun ;)

### Editing level

If you want to edit the current level, just press the ESC button.
You can see some blocks under the map. Use Number buttons, Arrow keys or your mouse to select one. Next click on that place on map, where you want to put your block. Use S button to save and ESC button to return to the game.
Enjoy!

![Level editing][le]
[le]: https://lh6.googleusercontent.com/-WUpqEKscoGM/VOW6X6UsxoI/AAAAAAAAA2o/bgXpdoj1z2I/w960-h715-no/edit.tiff
"Level editing"

## Themes

### Select a theme

In the project, folder you can see a file `config.conf`. Open this file and set `theme` variable equal to the folder theme name. You can see aviable themes in folder "themes".

ex.

```
theme = main
```

### Creating theme

Open terminal and type:

```
cd /path/to/project
mkdir themes/THEME_NAME_HERE
cp -a themes/main/* themes/THEME_NAME_HERE
```

Next you can edit images.
