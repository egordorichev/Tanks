all: linux
macos:
	g++ -I/usr/local/include -L/usr/local/lib -std=c++11 tanks.cpp -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer -o Tanks
linux:
	g++ -std=c++11 tanks.cpp -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer -o Tanks
windows:
	g++ -std=c++11tanks.cpp -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer -o Tanks
install:
	cp ./Tanks /usr/bin/
