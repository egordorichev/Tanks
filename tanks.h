#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h> 
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

SDL_Window *window;
SDL_Renderer *render;
SDL_Event event;
SDL_Texture *images[5];
TTF_Font *MainFont;
SDL_Color White = {255, 255, 255};
SDL_Surface *Text;
SDL_Texture *Texttex;
Mix_Chunk *chunks[5];
Mix_Music *music;
const Uint8 *keyboardState = SDL_GetKeyboardState(NULL);
bool running = true;
bool menu = true;
bool lselect = false;
bool playing = false;
bool editing = false;
int next = 3;
int points = 0;
int mouseX;
int mouseY;
string themepath;

/*class button{
	public:
		button();
		void check(SDL_Event *event, unsigned mouseX, unsigned mouseY);
		void setX(unsigned newX){ x = newX; };
		void setY(unsigned newY){ y = newY; };
		void setX(unsigned newH){ height = newH; };
		void setX(unsigned newW){ width = newW; };
	private:
		unsigned x;
		unsigned y;
		unsigned width;
		unsigned height;
		virtual void onClick();
		virtual void onHover();
};

button::button()
{

}

void button::check(SDL_Event *event, unsigned int mouseX, unsigned int mouseY)
{
	int mouseX, mouseY;
	SDL_GetMouseState(&mouseX, &mouseY);
	switch(event.type){
		case SDL_MOUSEBUTTONDOWN: if(mouseX >= x && mouseX <= x + width && mouseY >= y && mouseY <= y + width) onClick(); continue;
	}
}*/


class block{
	public:
		void setType(int nt){ type = nt; };
		int getType(){ return type; };
	private:
		int type;
};

block map[19][29];

enum vside{
	TOP = 1,
	DOWN = 2,
	LEFT = 3,
	RIGHT = 4
};

class point{
	public:	
		int x;
		int y;
};

vside randomSide(){
	srand(time(0));
	int randomn = rand() % 4 + 1;
	switch(randomn){
		case 1: return TOP; 
		case 2: return DOWN;
		case 3: return RIGHT;
		case 4: return LEFT;
	}
}

class explosion{
	public:
		explosion();
		int getX(){ return x; };
		int getY(){ return y; };
		void update();
		bool isActive(){ return active; };
		int getStep(){ return step; };
		void run(int rx, int ry);
	private:
		int x;
		int y;
		int step;
		bool active;
};

explosion::explosion(){
	x = 0;
	y = 0;
	active = false;
	step = 0;
}

void explosion::update(){
	step++;
	if(step >= 24){
		step = 0;
		active = false;
	}
}

void explosion::run(int rx, int ry){
	x = rx;
	y = rx;
	active = true;
}

class bullet{
	public:
		bullet();
		void activate(int direction, int x, int y, int nspeed);
		void unactivate(){ active = false; };
		bool isActive(){ return active; };
		bool update(int x, int y);
		int getXIW(){ return xIW; };
		int getYIW(){ return yIW; };
		int getXOS(){ return xOS; };
		int getYOS(){ return yOS; };
	private:
		int xIW;
		int yIW;
		int xOS;
		int yOS;
		int dir;
		int speed;
		bool active;
};


class badTank{
	public:
		badTank();
		void move(int x, int y);
		void fight();
		void update(int x, int y);		
		void spawn();
		void unactivate(){ active = false; };
		bool isActive(){ return active; }
		int getXIW(){ return xIW; };
		int getYIW(){ return yIW; };
		int getXOS(){ return xOS; };
		int getYOS(){ return yOS; };
		void setLives(int live){ lives = live; livemax = live; };
		vside getSide(){ return side; };
		void kill(){ lives = 0; }
		unsigned getLives(){ return lives; };
		unsigned getLiveMax(){ return livemax; };
		void takeLive() { lives--; };
		bullet Bullets[5];
	private:
		int xIW;
		int yIW;
		int xOS;
		int yOS;
		float speed;
		vside side;
		bool active;
		unsigned lives;
		unsigned livemax;
		point spawnPoint;
};

class tank{
	public:
		tank();
		void move(int direction);
		void fight();
		void update();
		int getXIW(){ return xIW; };
		int getYIW(){ return yIW; };
		int getXOS(){ return xOS; };
		int getYOS(){ return yOS; };
		unsigned getLives(){ return lives; };
		void kill(){ lives = 0; }
		vside getSide(){ return side; };
		void takeLive() { lives--; };
		bullet Bullets[5];
	private:
		int xIW;
		int yIW;
		int xOS;
		int yOS;
		float speed;
		vside side;
		unsigned lives;
		point spawnPoint;
};	

badTank::badTank()
{
	side = randomSide();
	speed = 2;
	lives = 3;
	livemax = lives;
	active = false;
}

void badTank::spawn()
{
	active = true;
	spawnPoint.y = rand() % 19 + 1;
	spawnPoint.x = rand() % 29 + 1;
	while(map[spawnPoint.y][spawnPoint.x].getType() == 5 && map[spawnPoint.y][spawnPoint.x].getType() == 7){
		spawnPoint.y = rand() % 19 + 1;
		spawnPoint.x = rand() % 29 + 1;
	}
	xOS = 0;
	yOS = 0;
	xIW = spawnPoint.x;
	yIW = spawnPoint.y;
}

void badTank::fight()
{
	int to;
	switch(side){
		case DOWN: to = -1; break; 
		case TOP: to = 1; break; 
		case LEFT: to = -2; break; 
		case RIGHT: to = 2; break; 
	}
	if(!Bullets[0].isActive())
		Bullets[0].activate(to, xIW, yIW, 6);
	else if(!Bullets[1].isActive())
		Bullets[1].activate(to, xIW, yIW, 6);
	else if(!Bullets[2].isActive())
		Bullets[2].activate(to, xIW, yIW, 6);
	else if(!Bullets[3].isActive())
		Bullets[3].activate(to, xIW, yIW, 6);
	else if(!Bullets[4].isActive())
		Bullets[4].activate(to, xIW, yIW, 6);
}

void badTank::move(int x, int y)
{	

	switch(side){
		case TOP:  if(yIW != 0 && map[yIW][xIW].getType() != 5 && map[yIW-1][xIW].getType() != 7){ yOS-=speed; } else{ side = randomSide(); }  break;
		case DOWN: if(yIW < 19 && map[yIW+1][xIW].getType() != 5 && map[yIW+1][xIW].getType() != 7){ yOS+=speed; } else{ side = randomSide(); } break;
		case RIGHT: if(xIW < 29 && map[yIW][xIW+1].getType() != 5 && map[yIW][xIW+1].getType() != 7){ xOS+=speed; } else{ side = randomSide(); } break;
		case LEFT: if(xIW != 0 && map[yIW][xIW].getType() != 5 && map[yIW][xIW].getType() != 7){ xOS-=speed;} else{ side = randomSide(); } break;	
		
	}

	if(xOS >= 32){ xOS = 0; xIW++; }
	if(yOS >= 32){ yOS = 0; yIW++; }
	if(xOS < 0){ xOS = 32; xIW--; }
	if(yOS < 0){ yOS = 32; yIW--; }
	
}

void badTank::update(int x, int y)
{
	if(active){

		if(lives == 0){
			active = false;

		}

		if(map[yIW][xIW].getType() == 4 && yOS == 0 && xOS == 0){
			xIW = spawnPoint.x;
			yIW = spawnPoint.y;
			yOS = 0;
			xOS = 0;
		} 

		if(xOS == 0 && yOS == 0){
			if(x == xIW){
				if(y > yIW){
					side = DOWN;
					fight();
				}
				else if(y < yIW){
					side = TOP;
					fight();
				}
			}
			if(y == yIW){
				if(x > xIW){
					side = RIGHT;
					fight();
				}
				else if(x < xIW){
					side = LEFT;
					fight();
				}
			}
		}	
		if(map[yIW][xIW].getType() == 4 && yOS == 0 && xOS == 0){
			xIW = spawnPoint.x;
			yIW = spawnPoint.y;
			yOS = 0;
			xOS = 0;
			side = TOP;
		}

		if(map[yIW][xIW].getType() == 2 || map[yIW][xIW].getType() == 3)
			speed = 1;
		else
			speed = 2;
	}
}

tank::tank(){
	spawnPoint.x = 15;
	spawnPoint.y = 18;
	while(map[spawnPoint.y][spawnPoint.x].getType() == 5 || map[spawnPoint.y][spawnPoint.x].getType() == 7){
		spawnPoint.y = rand() % 19 + 1;
		spawnPoint.x = rand() % 29 + 1;
	}
	xOS = 0;
	yOS = 0;
	xIW = spawnPoint.x;
	yIW = spawnPoint.y;
	side = TOP;
	speed = 2;
	lives = 5;
}

void tank::move(int direction){
	if(xOS == 0 && yOS == 0){
		switch(direction){
			case 1:  if(yIW != 0 && map[yIW-1][xIW].getType() != 5 && map[yIW-1][xIW].getType() != 7){ yOS-=speed; }  side = TOP;  break;
			case -1: if(yIW < 19 && map[yIW+1][xIW].getType() != 5 && map[yIW+1][xIW].getType() != 7){ yOS+=speed; }  side = DOWN; break;
			case 2: if(xIW < 29 && map[yIW][xIW+1].getType() != 5 && map[yIW][xIW+1].getType() != 7){ xOS+=speed; }  side = RIGHT; break;
			case -2: if(xIW != 0 && map[yIW][xIW-1].getType() != 5 && map[yIW][xIW-1].getType() != 7){ xOS-=speed;} side = LEFT; break;
		}
	}
	if(xOS >= 32){ xOS = 0; xIW++; }
	if(yOS >= 32){ yOS = 0; yIW++; }
	if(xOS < 0){ xOS = 32; xIW--; }
	if(yOS < 0){ yOS = 32; yIW--; }
}

void tank::update(){
	if(lives == 0)
		playing = false;
		running = false;
	if(xOS != 0 || yOS != 0){	
		switch(side){
			case TOP:  yOS-=speed;  break;
			case DOWN: yOS+=speed;  break;
			case RIGHT: xOS+=speed; break;
			case LEFT: xOS-=speed; break;
		}
	}
	if(xOS >= 32){ xOS = 0; xIW++; }
	if(yOS >= 32){ yOS = 0; yIW++; }
	if(xOS < 0){ xOS = 32; xIW--; }
	if(yOS < 0){ yOS = 32; yIW--; }
	
	if(map[yIW][xIW].getType() == 4 && yOS == 0 && xOS == 0){
		xIW = spawnPoint.x;
		yIW = spawnPoint.y;
		yOS = 0;
		xOS = 0;
	}

	if(map[yIW][xIW].getType() == 2 || map[yIW][xIW].getType() == 3)
		speed = 1;
	else
		speed = 2;

}

void tank::fight(){
	int to;
	switch(side){
		case DOWN: to = -1; break; 
		case TOP: to = 1; break; 
		case LEFT: to = -2; break; 
		case RIGHT: to = 2; break; 
	}

	if(!Bullets[0].isActive())
		Bullets[0].activate(to, xIW, yIW, 2);
	else if(!Bullets[1].isActive())
		Bullets[1].activate(to, xIW, yIW, 2);
	else if(!Bullets[2].isActive())
		Bullets[2].activate(to, xIW, yIW, 2);
	else if(!Bullets[3].isActive())
		Bullets[3].activate(to, xIW, yIW, 2);
	else if(!Bullets[4].isActive())
		Bullets[4].activate(to, xIW, yIW, 2);	
}

bullet::bullet(){
	unactivate();
}

void bullet::activate(int direction, int x, int y, int nspeed){
	active = true;
	dir = direction;
	xIW = x;
	yIW = y;
	yOS = 12;
	speed = nspeed;
	xOS = 12;
}

bool bullet::update(int x, int y){
	if(active){	
		switch(dir){
			case 1: yOS-=speed; if(yOS <= 0){ yOS = 32; yIW--; } break;
			case -1: yOS+=speed; if(yOS >= 32){ yOS = 0; yIW++; } break;
			case 2: xOS+=speed; if(xOS >= 32){ xOS = 0; xIW++; } break;
			case -2: xOS-=speed; if(xOS <= 0){ xOS = 32; xIW--; } break;
		}

		if(map[yIW][xIW].getType() == 5 ){ unactivate(); map[yIW][xIW].setType(8);}
		if(map[yIW][xIW].getType() == 7 ){ unactivate(); }
		if(xIW >= 30){ unactivate(); }
		if(xIW <= -1){ unactivate(); }
		if(yIW >= 20){ unactivate(); }
		if(yIW <= -1){ unactivate(); }
	

		if(x == xIW && y == yIW){
			unactivate();			
			return true;
		}
	}
	return false;
}

void init(){
	SDL_Init(SDL_INIT_VIDEO);
	window = SDL_CreateWindow("Tanks", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 960, 640, SDL_WINDOW_SHOWN);
	render = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	TTF_Init();
	MainFont = TTF_OpenFont("./fonts/PixelArt.ttf", 26);
	Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 2048 );
	ifstream conf;
	conf.open("config.conf");
	string var;
	string value;
	conf >> var >> value >> value;
	if(var == "theme"){
		themepath = "themes/" + value + "/";
	}
	conf.close();
}

void update(){
	SDL_RenderPresent(render);
	SDL_RenderClear(render);
}

void loadImage(int num, const char *path){
	images[num] = IMG_LoadTexture(render, path);
}

void drawImage(int num, int x, int y, int w, int h){
	SDL_Rect irect = {x, y, w, h};
	if(SDL_RenderCopy(render, images[num], NULL, &irect))
		cout << SDL_GetError();
}

void drawRotatedImage(int num, int x, int y, int w, int h, double angle){
	SDL_Rect irect = {x, y, w, h};
	if(SDL_RenderCopyEx(render, images[num], NULL, &irect, angle, NULL, SDL_FLIP_NONE))
		cout << SDL_GetError();
}

void drawTile(int num, int x, int y, int w, int h, int sx, int sy, int sw, int sh){
	SDL_Rect drect = {sx, sy, sw, sh};
	SDL_Rect irect = {x, y, w, h};
	SDL_RenderCopy(render, images[num], &irect, &drect);
}

void drawRotatedTile(int num, int x, int y, int w, int h, int sx, int sy, int sw, int sh, double angle){
	SDL_Rect drect = {sx, sy, sw, sh};
	SDL_Rect irect = {x, y, w, h};
	SDL_RenderCopyEx(render, images[num], &irect, &drect, angle, NULL, SDL_FLIP_NONE);
}

void print(const char *txt, int x, int y){
	Text = TTF_RenderText_Blended(MainFont, txt, White);
	Texttex = SDL_CreateTextureFromSurface(render, Text);
	int iW, iH;
	SDL_QueryTexture(Texttex, NULL, NULL, &iW, &iH);
	SDL_Rect drect = {x, y, iW, iH};
	SDL_RenderCopy(render, Texttex, NULL, &drect);
}

void print(int txt, int x, int y){
	char what[sizeof(txt)];
	sprintf(what, "%d", txt);
	Text = TTF_RenderText_Blended(MainFont, (const char *)what, White);
	Texttex = SDL_CreateTextureFromSurface(render, Text);
	int iW, iH;
	SDL_QueryTexture(Texttex, NULL, NULL, &iW, &iH);
	SDL_Rect drect = {x, y, iW, iH};
	SDL_RenderCopy(render, Texttex, NULL, &drect);
}

void loadMusic(const char* path){
	music = Mix_LoadMUS(path);
}

void playMusic(){
	Mix_PlayMusic(music, -1);
}

void stopMusic(){
	Mix_PauseMusic();
}

void loadChunk(int num, const char* path){
	chunks[num] = Mix_LoadWAV(path);
}

void playChunk(int num){
	Mix_PlayChannel(-1, chunks[num], 0);
}

void loadMap(const char *path){
	ifstream mf;
	mf.open(path);
	for(int i = 0; i <= 19; i++){
		for(int g = 0; g <= 29; g++){
			int b;
			mf >> b;
			map[i][g].setType(b);
		}
	}
	mf.close();
}

void saveMap(){
	ofstream mf("maps/2.map"); 
	for(int i = 0; i <= 19; i++){
		for(int g = 0; g <= 29; g++){
			char s = map[i][g].getType() + '0';
			mf << s << " ";
		}
		mf << endl;
	}
  mf.close();
}

bool fileExists(const char *path){
	ifstream file(path);
	if(!file)
		return true;
	else
		return false;
}

