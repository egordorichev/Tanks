#include "tanks.h"

int main(){
	
init();
loadImage(0, (themepath + "assets.png").c_str());
loadImage(1, (themepath + "boom.png").c_str());
loadImage(2, (themepath + "selected.png").c_str());
loadImage(3, "images/menu.png");
loadMusic("music/battle.mp3");
loadChunk(0, "music/explosion.wav");

tank Tank;
badTank Bad[50];

badTank Boss;
 
const int numTanks = 5;

Bad[0].spawn();
Bad[1].spawn();
Bad[2].spawn();
Bad[3].spawn();
Bad[4].spawn();

explosion Boom[50];

playMusic();

while(running){

SDL_SetWindowSize(window, 960, 640);

unsigned mselected = 1;
unsigned level = 1;

while(menu){

	if(!lselect){
	
	drawImage(3, 0, 0, 960, 640);

	print("Play", 370, 200);
	print("Exit", 370, 250);
	
	switch(mselected){
		case 1: drawTile(0, 0, 0, 32, 32, 320, 200, 32, 32); break;
		case 2: drawTile(0, 0, 0, 32, 32, 320, 250, 32, 32); break;
	}
	
	while(SDL_PollEvent(&event)){
		SDL_GetMouseState(&mouseX, &mouseY);
		switch(event.type){
			case SDL_QUIT: menu = false; running = false; break;
			case SDL_KEYDOWN: 
				switch(event.key.keysym.sym){
					case SDLK_DOWN: if(mselected != 2) mselected++; break;
					case SDLK_UP: if(mselected != 1) mselected--; break;
					case SDLK_RETURN: 
						switch(mselected){						
							case 1: lselect = true; break;
							case 2: menu = false; running = false; break;
						} break;
				} break;
			case SDL_MOUSEBUTTONDOWN:	
			break;
		}
	}
	}
	else{
	
	print("Select level ", 370, 200);

	print(level,  630, 200);
		
	while(SDL_PollEvent(&event)){
		SDL_GetMouseState(&mouseX, &mouseY);
		switch(event.type){
			case SDL_QUIT: menu = false; running = false; break;
			case SDL_KEYDOWN: 
				switch(event.key.keysym.sym){
					case SDLK_DOWN: level++; break;
					case SDLK_UP: level--; break;
					case SDLK_RETURN: string sl = to_string(level); loadMap(("maps/" + sl + ".map").c_str()); menu = false; playing = true; break;
				}
			case SDL_MOUSEBUTTONDOWN:	
			break;
		}
	}	
		
	}
	update();
	SDL_Delay(1000/60);
}

while(playing){

	for(int i = 0; i <= 19; i++){
		for(int g = 0; g <= 29; g++){
			drawTile(0, (map[i][g].getType()-1)*32, 32, 32, 32, g*32, i*32, 32, 32);
		}
	}
	for(int i = 0; i < 5; i++){
		if(Tank.Bullets[i].isActive()){
			if(Boss.isActive()){
				if(Tank.Bullets[i].update(Boss.getXIW(),Boss.getYIW())){
						Boss.takeLive();
				}
			}
			for(int g = 0; g < 50; g++){
				if(Bad[g].isActive()){
					if(map[Bad[g].getYIW()][Bad[g].getXIW()].getType() == 6 && Bad[g].getYOS() == 0 && Bad[g].getXOS() == 0){
						Boom[g].run(Bad[g].getXIW()*32, Bad[g].getYIW()*32);
						playChunk(0);
						map[Bad[g].getYIW()][Bad[g].getXIW()].setType(8);
						Bad[g].kill();
						Bad[g].unactivate();
						/*if(next != 50){
							Bad[next].spawn();
							next++;
						}
						else{
							Boss.spawn();
							Boss.setLives(10);
						}*/
					}
					if(Tank.Bullets[i].update(Bad[g].getXIW(), Bad[g].getYIW())){
						Bad[g].takeLive();
						points++;
						if(Bad[g].getLives() == 0){
							Boom[g].run(Bad[g].getXIW()*32, Bad[g].getYIW()*32);
							playChunk(0);
							Bad[g].unactivate();
							/*if(next != 50){
								Bad[next].spawn();
								next++;
							}
							else{
								Boss.spawn();
								Boss.setLives(10);
							}*/
							points += 7;
						}
					}
				}
			}	
			drawTile(0, 128, 0, 6, 6, Tank.Bullets[i].getXIW()*32+Tank.Bullets[i].getXOS(), Tank.Bullets[i].getYIW()*32+Tank.Bullets[i].getYOS(), 8, 8);
		}
	}

	for(int u = 0; u < 50; u++){
		if(Bad[u].isActive()){
			for(int i = 0; i < 5; i++){
				if(Bad[u].Bullets[i].isActive()){
					if(Bad[u].Bullets[i].update(Tank.getXIW(), Tank.getYIW())){
							Tank.takeLive();
							Bad[u].Bullets[i].unactivate();
					}
					drawTile(0, 128, 0, 6, 6, Bad[u].Bullets[i].getXIW()*32+Bad[u].Bullets[i].getXOS(), Bad[u].Bullets[i].getYIW()*32+Bad[u].Bullets[i].getYOS(), 8, 8);
				}
			}

			switch(Bad[u].getSide()){
				case RIGHT: drawTile(0, 32, 0, 32, 32, Bad[u].getXIW()*32+Bad[u].getXOS(), Bad[u].getYIW()*32+Bad[u].getYOS(), 32, 32); break;
				case LEFT: drawRotatedTile(0, 32, 0, 32, 32, Bad[u].getXIW()*32+Bad[u].getXOS(), Bad[u].getYIW()*32+Bad[u].getYOS(), 32, 32, 180.0); break;
				case TOP: drawRotatedTile(0, 32, 0, 32, 32, Bad[u].getXIW()*32+Bad[u].getXOS(), Bad[u].getYIW()*32+Bad[u].getYOS(), 32, 32, 270.0); break;
				case DOWN: drawRotatedTile(0, 32, 0, 32, 32, Bad[u].getXIW()*32+Bad[u].getXOS(), Bad[u].getYIW()*32+Bad[u].getYOS(), 32, 32, 90.0); break;
			}
	
			if(Bad[u].getLiveMax() == Bad[u].getLives())	
				drawTile(0, 96, 6, 32, 6, Bad[u].getXIW()*32+Bad[u].getXOS(), Bad[u].getYIW()*32+Bad[u].getYOS()-10, 32/Bad[u].getLiveMax()*Bad[u].getLives()+1, 6);
			else if(Bad[u].getLives() == 1)
				drawTile(0, 96, 18, 32, 6, Bad[u].getXIW()*32+Bad[u].getXOS(), Bad[u].getYIW()*32+Bad[u].getYOS()-10, 32/Bad[u].getLiveMax()*Bad[u].getLives()+1, 6);
			else
				drawTile(0, 96, 12, 32, 6, Bad[u].getXIW()*32+Bad[u].getXOS(), Bad[u].getYIW()*32+Bad[u].getYOS()-10, 32/Bad[u].getLiveMax()*Bad[u].getLives()+1, 6);

			drawTile(0, 96, 0, 32, 6, Bad[u].getXIW()*32+Bad[u].getXOS(), Bad[u].getYIW()*32+Bad[u].getYOS()-10, 32, 6);

			Bad[u].move(Tank.getXIW(), Tank.getYIW());
			Bad[u].update(Tank.getXIW(), Tank.getYIW());
		}

	}

		if(Boss.isActive()){
			for(int i = 0; i < 5; i++){
				if(Boss.Bullets[i].isActive()){
					if(Boss.Bullets[i].update(Tank.getXIW(), Tank.getYIW())){
							Tank.takeLive();
							Boss.Bullets[i].unactivate();
					}
					drawTile(0, 128, 0, 6, 6, Boss.Bullets[i].getXIW()*32+Boss.Bullets[i].getXOS(), Boss.Bullets[i].getYIW()*32+Boss.Bullets[i].getYOS(), 6, 6);
				}
			}

			switch(Boss.getSide()){
				case RIGHT: drawTile(0, 64, 0, 32, 32, Boss.getXIW()*32+Boss.getXOS(), Boss.getYIW()*32+Boss.getYOS(), 32, 32); break;
				case LEFT: drawRotatedTile(0, 64, 0, 32, 32, Boss.getXIW()*32+Boss.getXOS(), Boss.getYIW()*32+Boss.getYOS(), 32, 32, 180.0); break;
				case TOP: drawRotatedTile(0, 64, 0, 32, 32, Boss.getXIW()*32+Boss.getXOS(), Boss.getYIW()*32+Boss.getYOS(), 32, 32, 270.0); break;
				case DOWN: drawRotatedTile(0, 64, 0, 32, 32, Boss.getXIW()*32+Boss.getXOS(), Boss.getYIW()*32+Boss.getYOS(), 32, 32, 90.0); break;
			}
	
			if(Boss.getLiveMax() == Boss.getLives())	
				drawTile(0, 96, 6, 32, 6, Boss.getXIW()*32+Boss.getXOS(), Boss.getYIW()*32+Boss.getYOS()-10, 32/Boss.getLiveMax()*Boss.getLives()+1, 6);
			else if(Boss.getLives() == 1)
				drawTile(0, 96, 18, 32, 6, Boss.getXIW()*32+Boss.getXOS(), Boss.getYIW()*32+Boss.getYOS()-10, 32/Boss.getLiveMax()*Boss.getLives()+1, 6);
			else
				drawTile(0, 96, 12, 32, 6, Boss.getXIW()*32+Boss.getXOS(), Boss.getYIW()*32+Boss.getYOS()-10, 32/Boss.getLiveMax()*Boss.getLives()+1, 6);

			drawTile(0, 96, 0, 32, 6, Boss.getXIW()*32+Boss.getXOS(), Boss.getYIW()*32+Boss.getYOS()-10, 32, 6);

			Boss.move(Tank.getXIW(), Tank.getYIW());
			Boss.update(Tank.getXIW(), Tank.getYIW());
		}

		if(map[Tank.getYIW()][Tank.getXIW()].getType() == 6 && Tank.getYOS() == 0 && Tank.getXOS() == 0){
			Boom[9].run(Tank.getXIW()*32, Tank.getYIW()*32);			
			playChunk(0);
			Tank.kill();
		}
	
	switch(Tank.getSide()){
		case RIGHT: drawTile(0, 0, 0, 32, 32, Tank.getXIW()*32+Tank.getXOS(), Tank.getYIW()*32+Tank.getYOS(), 32, 32); break;
		case LEFT: drawRotatedTile(0, 0, 0, 32, 32, Tank.getXIW()*32+Tank.getXOS(), Tank.getYIW()*32+Tank.getYOS(), 32, 32, 180.0); break;
		case TOP: drawRotatedTile(0, 0, 0, 32, 32, Tank.getXIW()*32+Tank.getXOS(), Tank.getYIW()*32+Tank.getYOS(), 32, 32, 270.0); break;
		case DOWN: drawRotatedTile(0, 0, 0, 32, 32, Tank.getXIW()*32+Tank.getXOS(), Tank.getYIW()*32+Tank.getYOS(), 32, 32, 90.0); break;
	}

	for(int i = 0; i < 10; i++){
		if(Boom[i].isActive()){
			drawTile(1, Boom[i].getStep()*64, 0, 64, 64, Boom[i].getX(), Boom[i].getY(), 64, 64);
			Boom[i].update();
		}
	}

	for(int i = 0; i < Tank.getLives(); i++){
		drawTile(0, 128, 6, 22, 22, i*16+i*5+850, 5, 22, 22);
	}

	print(points, 5, 5);

	while(SDL_PollEvent(&event)){
		switch(event.type){
			case SDL_QUIT: running = false; return 0; break;
			case SDL_KEYDOWN: 
				switch(event.key.keysym.sym){
					case SDLK_UP: Tank.move(1); continue;
					case SDLK_w: Tank.move(1); continue;
					case SDLK_a: Tank.move(-2); continue;
					case SDLK_LEFT: Tank.move(-2); continue;
					case SDLK_s: Tank.move(-1); continue;
					case SDLK_DOWN: Tank.move(-1); continue;
					case SDLK_d: Tank.move(2); continue;
					case SDLK_RIGHT: Tank.move(2); continue;
					case SDLK_SPACE: if (event.key.repeat){ break; } Tank.fight(); continue;
					case SDLK_F1: editing = true; playing = false; continue;
				}
			break;
		}
	}

	Tank.update();

	update();
	SDL_Delay(1000/30);
}

SDL_SetWindowSize(window, 960, 693);
unsigned selected = 1;

while(editing){

	for(int i = 0; i <= 19; i++){
		for(int g = 0; g <= 29; g++){
			drawTile(0, (map[i][g].getType()-1)*32, 32, 32, 32, g*32, i*32, 32, 32);
		}
	}
	drawTile(0, 3*32, 32, 2, 2, (selected-1)*42+5, 645, 42, 42);
	for(int i = 0; i <= 8; i++){
		drawTile(0, i*32, 32, 32, 32, i*32+(i+1)*10, 650, 32, 32);
	}

	SDL_GetMouseState(&mouseX, &mouseY);
	
	if(mouseY <= 640)
		drawImage(2, mouseX/32*32, mouseY/32*32, 32, 32);

	while(SDL_PollEvent(&event)){
		switch(event.type){
			case SDL_QUIT: editing = false; running = false; break;
			case SDL_KEYDOWN: 
				switch(event.key.keysym.sym){
					case SDLK_s: saveMap(); break;
					case SDLK_LEFT: selected--; break;
					case SDLK_RIGHT: selected++; break;
					case SDLK_F1: saveMap(); editing = false; running = true; playing = true; break;
					case SDLK_1: selected = 1; break;
					case SDLK_2: selected = 2; break;
					case SDLK_3: selected = 3; break;
					case SDLK_4: selected = 4; break;
					case SDLK_5: selected = 5; break;
					case SDLK_6: selected = 6; break;
					case SDLK_7: selected = 7; break;
					case SDLK_8: selected = 8; break;
				} break;
			case SDL_MOUSEBUTTONDOWN:	
				if(mouseY <= 640){
					float x = mouseX/32;
					float y = mouseY/32;
					map[(int)y][(int)x].setType(selected);
				}
				if(mouseY >= 640){
					float x = mouseX/42;
					selected = x+1;
				}
			break;
		}
	}

	update();
	SDL_Delay(1000/60);
}

}

TTF_CloseFont(MainFont);
SDL_Quit();

return 0;

}
