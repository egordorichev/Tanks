#!/bin/bash
wget -q http://www.libsdl.org/release/SDL2-2.0.0.tar.gz
tar -zxvf SDL2-2.0.0.tar.gz
cd SDL2-2.0.0
./configure --prefix="$(pwd)/../" && make && make install
wget -q http://www.libsdl.org/projects/SDL_ttf/release/SDL2_ttf-2.0.12.tar.gz
tar -zxvf SDL2_ttf-2.0.12.tar.gz
cd SDL2_ttf-*
./configure --prefix="$(pwd)/../" && make && make install
wget -q http://www.libsdl.org/projects/SDL_mixer/release/SDL2_mixer-2.0.0.tar.gz
tar -zxvf SDL2_mixer-2.0.0.tar.gz
cd SDL2_mixer-*
./configure --prefix="$(pwd)/../" && make && make install
wget -q http://www.libsdl.org/projects/SDL_image/release/SDL_image-2.0.0.tar.gz
tar -zxvf SDL_image-2.0.0.tar.gz
cd SDL_image-*
./configure --prefix="$(pwd)/../" && make && make install
